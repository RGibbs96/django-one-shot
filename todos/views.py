from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.db.models import Count
from todos.forms import TodoListForm, TodoItemForm


def all_lists(request):
    lists = TodoList.objects.annotate(  # annotate adds an additional attribute (numitems) I can call
        numitems=Count("items")
    )
    context = {
        "all_lists": lists,
    }
    return render(request, "todo_lists/list.html", context)


def show_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }
    return render(request, "todo_lists/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            id = new_list.id
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todo_lists/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todo_lists/edit.html", context)


def delete_list(request, id):
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todo_lists/createitem.html", context)


def edit_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todo_lists/updateitem.html", context)
